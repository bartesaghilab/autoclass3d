import os, argparse, glob, shutil
import subprocess

parser = argparse.ArgumentParser(description='Prepare serial classification runs')
parser.add_argument("-maxk", "--maxk", help="Maximum number of classes to run", type=int, required=True)
parser.add_argument("-dataset", "--dataset", help="Dataset name (prefix used for cistem stack and par files)", required=True)
parser.add_argument("-iter", "--iter", help="Iteration number to initialize classifcation", type=int, required=True)
parser.add_argument("-stack", "--stack", help="Particle stack name prefix (in cistem project folder)", required=True)
args = parser.parse_args()

for k in range(1, args.maxk + 1):
    
    # generate a new directory for each run
    dir_name = "C%d_data/cistem/maps/" % k
    pattern = os.path.split( glob.glob( 'cistem/maps/%s_*_%02d.par' % (args.dataset, args.iter ) )[-1] )[-1][:-11]
    os.mkdirs(dir_name)
    source = pattern + '_r01_%02d.*' % args.iter
    
    # copy necessary par files, mrc references, res, and fsc files to this run's folder 
    source_files = sorted(
        glob.glob("cistem/maps/" + source) 
        + glob.glob("cistem/maps/" + args.dataset + "*.txt") 
        + glob.glob("cistem/maps/" + args.dataset + "*_half*"))
    for files in source_files:    
        target_file = 'C%d_'% k + os.path.split(files)[-1]
        target = dir_name + target_file
        shutil.copy2(source, target, follow_symlinks=True)

    # create sym link to particle stack to avoid generating duplicates
    stack_file = "cistem/" + args.stack
    target_stack = "C%d_data/cistem/" % k + 'C%d_'% k + args.stack
    os.symlink(stack_file, target_stack)

    # make sure all runs use the same cistem settings
    shutil.copy2("cistem/cistem.config", "C%d_data/cistem/cistem.config")

    # launch 25 iterations of 3D classification using cisTEM (this can be done within the GUI)

    os.chdir("C%d_data/cistem/")
    iterations = args.iter + 25
    particle_stack = 'C%d_'% k + args.stack
    command = f"cistem_3D_classification_command -iter {args.iter + 1} -maxiter {iterations} -classes {k} -dataset {particle_stack}"
    print(command)
    subprocess.getoutput(command)

    # go back to project directory
    os.chdir("../../")