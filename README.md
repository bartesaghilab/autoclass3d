# Introduction

`autoclass3d` is a set of Python routines to implement data-driven determination of number of discrete conformations in single-particle cryo-EM as described in [Zhou et al., Computer Methods and Programs in Biomedicine, Special Issue of Computational Methods for Three-Dimensional Electron Microscopy (3DEM), 2022](https://doi.org/10.1016/J.CMPB.2022.106892).

The code is developed and maintained by the [Bartesaghi Lab](http://cryoem.cs.duke.edu) at Duke University.

# Requirements

A list of requirements is included in the file ``requirements.txt``. A conda enviroment can be generated using the following command:

```bash
conda create --name autoclass3d --file requirements.txt -c defaults -c conda-forge
```

The newly created enviroment can be activated using:
```bash
conda activate autoclass3d
```

# Usage

1. ``generate_serial_runs.py`` is used to prepare the 3D classification runs using increasing number of classes. This command prints a list of available options:
```
python generate_serial_runs.py -h
```

**NOTE**: This script does not actually run 3D classification, instead, that should be done using the [cisTEM](https://cistem.org/) package.

2. ``generate_plots_pattern.py`` is used to: 
    - Parse the parameter files resulting from the 3D classification runs
    - Produce a plot of the average variance of cisTEM scores

This command prints a list of available options:
```bash
python generate_plots_pattern.py -h
```