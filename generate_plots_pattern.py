import os, argparse, numpy, glob

import matplotlib
matplotlib.use('Agg')

parser = argparse.ArgumentParser(description='Pre-process micrograph')
parser.add_argument("-path", "--path", help='Path to cisTEM runs', required=True)
parser.add_argument("-runs", "--runs", help='number of classes', default=2, type=int)
parser.add_argument("-iter", "--iter", help='Last iteration', default=30, type=int)
parser.add_argument("-occ", "--occ", help='Max ocupancy', default=50, type=int)
parser.add_argument("-power", "--power", help='Exponent', default=2, type=int)
parser.add_argument("-stack", "--stack", help='stack main name', default='', type=str)
args = parser.parse_args()

# go to working directory
os.chdir(args.path)

values = numpy.zeros( args.runs )

for i in range(1, args.runs+1 ):
    pardir = 'C%d_data/cistem/maps/' % i
    total = 0
       
    pattern = os.path.split( glob.glob( 'C%d_data/cistem/maps/*%s_*_%d.par' % ( i, args.stack, args.iter ) )[-1] )[-1][:-11]

    for j in range(1,i+1):
        parfile = pardir + 'C%d_' % i + pattern + '_r%02d_%d.par' % ( j, args.iter )
        parfile = pardir + pattern + '_r%02d_%d.par' % ( j, args.iter )
        print('Using parfile ', parfile)
        prs = numpy.array( [ line[121:128] for line in file(parfile) if not line.startswith('C') and numpy.isfinite( float(line[121:128]) ) ], dtype=float )
        occ = numpy.array( [ line[92:99] for line in file(parfile) if not line.startswith('C') and numpy.isfinite( float(line[92:99]) ) ], dtype=float )
        if i > 1:
            current = prs[ occ >= args.occ ]
        else:
            current = prs
        mean = current.mean()
        if args.power > 0:
            increment = numpy.sqrt( numpy.power( ( current - mean ), args.power ).mean() )
        else:
            increment = current.mean()
        total += increment
        print('\tC%d' % j, mean, increment, len(current))

    print('C%d' % i, total, total/i)
    values[i-1] = total/i

import matplotlib.pyplot as plt
import seaborn as sns
sns.set(style="darkgrid")

plt.figure(figsize=(4,10))
ax = sns.barplot(x=values, y=range(1,args.runs+1), orient='h')
plt.plot( values, range(args.runs),'-o', label = args.path )
plt.xlabel('Mean variance',fontsize=20)
plt.ylabel('Number of classes',fontsize=20)

ax.tick_params(axis='both', labelsize=20)

plt.savefig( args.path + args.stack + '_occ_' + str(args.occ) + '_iter_' + str(args.iter) + '_runs' + str(args.runs) + '_power' + str(args.power) + '.pdf' )
